package shop.model.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import
        org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import shop.model.entities.Category;
import shop.model.entities.Product;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ProductsRepository extends
        PagingAndSortingRepository<Product, Integer> {
    List<Product> findByCategory_Id(int category_id);

    List<Product> findByCategory_Id(int category_id, Pageable
            pageable);

    Page<Product> findByCategoryInAndPriceBetween(
            List<Category> categories,
            BigDecimal minPrice,
            BigDecimal maxPrice,
            Pageable pageable);

    Page<Product> findByCategoryInAndPriceBetweenAndNameContaining(
            List<Category> categories,
            BigDecimal minPrice,
            BigDecimal maxPrice,
            String searchPhrase,
            Pageable pageable);
}